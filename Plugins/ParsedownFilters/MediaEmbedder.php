<?php

declare(strict_types=1);

namespace Zaplog\Plugins\ParsedownFilters {

    use ContentSyndication\HtmlMetadata;
    use Exception;
    use Zaplog\Exception\UserException;
    use Zaplog\Plugins\AbstractParsedownFilter;

    // To avoid copyright claims we must:
    // - only use publically availabe images
    // - only deep link to images
    // https://law.stackexchange.com/questions/38575/who-is-infringing-copyright-when-hotlinking-is-involved
    // We satisfy these conditions if we only deeplink to public URL's og:image

    class MediaEmbedder extends AbstractParsedownFilter
    {
        function __invoke(array $element): array
        {
            $getEmbedLink = function (string $url): ?array {
                // https://www.youtube.com/watch?v=UHkjxowYUdg
                if (preg_match("/.*youtube\.com\/watch.*v=([a-zA-Z0-9_-]+)(&t=(\d+))?/", $url, $matches) === 1) {
                    return ["https://www.youtube.com/embed/$matches[1]" . (empty($matches[3]) ? "" : "?start=$matches[3]" ) , "youtube video"];
                }
                if (preg_match("/youtu.be\/([a-zA-Z0-9_-]+)(\?t=(\d+))?/", $url, $matches) === 1) {
                    return ["https://www.youtube.com/embed/$matches[1]" . (empty($matches[3]) ? "" : "?start=$matches[3]" ), "youtube video"];
                }
                // https://open.spotify.com/show/4rOoJ6Egrf8K2IrywzwOMk
                if (preg_match("/.*open\.spotify\.com\/episode\/([a-zA-Z0-9_-]+)/", $url, $matches) === 1) {
                    return ["https://open.spotify.com/embed/episode/$matches[1]", "spotify audio"];
                }
                // https://www.bitchute.com/video/ftihsfWPhzAp/
                if (preg_match("/.*bitchute\.com\/video\/([a-zA-Z0-9_-]+)\//", $url, $matches) === 1) {
                    return ["https://www.bitchute.com/embed/$matches[1]/", "bitchute video"];
                }
                // https://odysee.com/What-is-graphene-oxide:b78c43bd498f180b76ee8bbaae9c560ee9b34c98
                if (preg_match("/.*odysee.com\/([a-zA-Z0-9_-]+):([a-zA-Z0-9]+)/", $url, $matches) === 1) {
                    return ["https://odysee.com/$/embed/$matches[1]/$matches[2]", "odysee video"];
                }
                // https://vimeo.com/574675111
                if (preg_match("/.*vimeo\.com\/([0-9]+)/", $url, $matches) === 1) {
                    return ["https://player.vimeo.com/video/$matches[1]/", "vimeo video"];
                }
                // https://www.docdroid.net/M4dJCZc/schwab2020-pdf
                if (preg_match("/.*docdroid.net\/([a-zA-Z0-9_-]+)\/(.+)/", $url, $matches) === 1) {
                    return [$url, "docdroid pdf"];
                }
                // https://www.scribd.com/doc/19098316/Whistleblower-Sibel-Edmonds-Ohio-deposition-Schmidt-v-Krikorian
                if (preg_match("/.*scribd.com\/doc\/([a-zA-Z0-9_-]+)\/(.+)/", $url, $matches) === 1) {
                    return ["https://www.scribd.com/embeds/$matches[1]/content", "scribd"];
                }
                // https://www.dailymotion.com/video/x87005y?playlist=x6lgtp
                if (preg_match("/.*dailymotion.com\/video\/([a-zA-Z0-9_-]+)/", $url, $matches) === 1) {
                    return ["https://www.dailymotion.com/embed/video/$matches[1]", "scribd"];
                }
                // https://sendvid.com/3xye8nqw
                if (preg_match("/.*sendvid\.com\/([a-zA-Z0-9_-]+)/", $url, $matches) === 1) {
                    return ["https://sendvid.com/embed/$matches[1]/", "sendvid video"];
                }
                // https://disk.yandex.ru/i/rONqsTtOe1aTaQ
                if (preg_match("/.*disk\.yandex\.ru\/i\/([a-zA-Z0-9_-]+)/", $url, $matches) === 1) {
                    return ["https://frontend.vh.yandex.ru/player/$matches[1]", "yandex video"];
                }
                return null;
            };

            // the Parsedown parser recognized and translated Markdown image syntax, we intercept it
            if (strcmp($element['name'], "img") === 0 and isset($element['attributes']['src'])) {

                try {
                    $metadata = (new HtmlMetadata)($element['attributes']['src']);
                    $embedurl = $metadata['embedurl'];

                    // EMBEDS: for specific domains we will use Markdown image-syntax to embed media
                    if (empty($embedurl)) {
                        /** @noinspection PhpUnusedLocalVariableInspection */
                        [$embedurl, $class] = $getEmbedLink($metadata["url"]);
                    }
                    if (!empty($embedurl)) {

                        return [
                            "name" => "iframe",
                            "text" => html_entity_decode($metadata['title'] ?? ""), // forces Parsedown parser to insert a closing tag
                            "attributes" => [
                                "title" => html_entity_decode($metadata['title'] ?? ""),
                                "src" => $embedurl,
                                // "class" => in_array($element['text'] ?? "", ["video", "video large", "video wide"]) ? $element['text'] : "video"
                                "class" => "video",
                            ],
                        ];
                    }

                    // IMAGES: other domains -> get the og:image (no copyright) through metadata-inspection
                    if (!empty($metadata['image'])) {

                        return [
                            "name" => "img",
                            "attributes" => [
                                "title" => html_entity_decode($metadata['title'] ?? ""),
                                "src" => $metadata['image'],
                                // "class" => in_array($element['text'] ?? "", ["image small", "image", "image large", "image wide"]) ? $element['text'] : "image",
                                "class" => "image",
                            ],
                        ];
                    }

                } catch (Exception) {
                    throw new UserException("Invalid media source: " . $element['attributes']['src']);
                }

            }
            return $element;
        }
    }
}
