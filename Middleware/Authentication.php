<?php

declare(strict_types=1);

namespace Zaplog\Middleware {

    use Ahc\Jwt\JWT;
    use Ahc\Jwt\JWTException;
    use Atrox\Haikunator;
    use Exception;
    use Multiavatar;
    use Psr\Http\Message\ResponseInterface;
    use Psr\Http\Message\ServerRequestInterface;
    use SlimRestApi\Infra\Db;
    use SlimRestApi\Infra\Password;
    use Zaplog\Exception\UserException;

    class Authentication
    {
        private static int $channelid = 0;
        private bool $throwexception;

        // ---------------------------------------------------------------------------------------
        // Create a JWT object, initializes and store the secret in apcu
        // ---------------------------------------------------------------------------------------

        static private function createJwt(): JWT
        {
            // this is not a password, just a key for apcu value (md5("jwt_secret"))
            static $key = "9182e15a2164dd4c3f538f2becdf3a1f";
            $secret = apcu_fetch($key);
            if (empty($secret)) {
                // fresh server start, must generate a secret
                apcu_add($key, password::randomMD5());
                // get key from apcu
                $secret = apcu_fetch($key);
                if (empty($secret)) {
                    throw new Exception("critical apcu error, could not add/fetch key");
                }
            }
            return new JWT($secret, 'HS256', 60 * 60 * 24 * 7);
        }

        // ---------------------------------------------------------------------------------------
        // Override of the parent calls, decorates the original call. Returns logged in channel
        // ---------------------------------------------------------------------------------------

        static public function getChannelId(): int
        {
            return self::$channelid;
        }

        // ---------------------------------------------------------------------------------------
        // Explicit logout by client
        // ---------------------------------------------------------------------------------------

        static public function deleteSession(): void
        {
            // nothing yet
        }

        // ----------------------------------------------
        // 2FA action, creates a session token.
        // Decorates the parent class method.
        // ----------------------------------------------

        static public function updateIdentity(string $newemail, int $channelid): array
        {
            // for now only accept email id's
            $newemail = filter_var(filter_var(strtolower($newemail), FILTER_SANITIZE_EMAIL), FILTER_VALIDATE_EMAIL);
            (new UserException("Invalid userid"))($newemail !== false);
            Db::execute("UPDATE channels SET userid=MD5(:newemail) WHERE id=:id", [":newemail" => $newemail, ":id" => $channelid]);
            // return a login / JWT
            $channel = Db::fetch("SELECT * FROM channels WHERE id=:id", [":id" => $channelid]);
            $token = (self::createJwt())->encode(["channelid" => $channelid]);
            return [
                "token" => ['X-Session-Token' => $token],
                "channel" => $channel,
            ];
        }

        // ----------------------------------------------
        // 2FA action, creates a session token.
        // Decorates the parent class method.
        // ----------------------------------------------

        static public function createSession(string $email): array
        {
            // for now only accept email id's
            $email = filter_var(filter_var(strtolower($email), FILTER_SANITIZE_EMAIL), FILTER_VALIDATE_EMAIL);
            (new UserException("Invalid userid"))($email !== false);
            $userid = md5($email);
            $channel = Db::fetch("SELECT * FROM channels WHERE userid=:userid", [":userid" => $userid]);

            // if we see a new user, we create a new channel
            if (empty($channel)) {
                $channelname = Haikunator::haikunate();
                $avatar = "data:image/svg+xml;base64," . base64_encode((new Multiavatar)($channelname, null, null));
                Db::execute("INSERT channels(userid,name,avatar) VALUES (:userid,:name,:avatar)",
                    [
                        ':userid' => $userid,
                        ':name' => $channelname,
                        ':avatar' => $avatar,
                    ]);
                $channel = Db::fetch("SELECT * FROM channels WHERE userid=:userid", [":userid" => $userid]);
            }

            // create JWT token
            $token = (self::createJwt())->encode(["channelid" => $channel->id]);
            return [
                "token" => ['X-Session-Token' => $token],
                "channel" => $channel,
            ];
        }

        public function __construct(bool $throwexception = true)
        {
            $this->throwexception = $throwexception;
        }

        public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next): ResponseInterface
        {
            try {
                try {
                    // extract token from header
                    $value = $request->getHeader('Authorization')[0] ?? false;
                    if (empty($value)) {
                        throw new JWTException;
                    }
                    $jwt = substr($value, strlen("Bearer "));
                    if (empty($jwt)) {
                        throw new JWTException;
                    }
                    // check the JWT
                    assert(!empty(preg_match("@^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$@", $jwt)));
                    self::$channelid = (self::createJwt())->decode($jwt)["channelid"];
                    assert(is_integer(self::$channelid));
                    // return a new token
                    $response = $response->withHeader("X-Session-Token", (self::createJwt())->encode(["channelid" => self::$channelid]));

                } catch (JWTException) {
                    if ($this->throwexception) {
                        throw new JWTException;
                    }
                }
                return $next($request, $response);

            } catch (JWTException) {
                // deny authorization
                return $response
                    ->withJson("Invalid token in Authorization bearer header")
                    ->withStatus(401)
                    // we must return XBasic (not Basic) to prevent clients from opening the AUTH dialog
                    ->withHeader('WWW-Authenticate', 'XBasic realm=api');
            }
        }
    }
}